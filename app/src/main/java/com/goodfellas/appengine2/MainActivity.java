package com.goodfellas.appengine2;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void function(View view) {
        EditText editText = (EditText) findViewById(R.id.name);
        String s = editText.getText().toString();
        editText = (EditText) findViewById(R.id.surname);
        s += editText.getText().toString();
        Pair<Context,String> pair = new Pair<Context, String>(this, s);
        new PutDataStore().execute(pair);

    }
}
